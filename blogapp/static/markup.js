


var toolbarOptions = {
        container: [
          ['bold', 'italic', 'underline', 'strike'],
          ['blockquote', 'code-block'],
          [{ 'header': 1 }, { 'header': 2 }],
          [{ 'list': 'ordered' }, { 'list': 'bullet' }],
          [{ 'script': 'sub' }, { 'script': 'super' }],
          [{ 'indent': '-1' }, { 'indent': '+1' }],
          [{ 'direction': 'rtl' }],
          [{ 'header': [1, 2, 3, 4, 5, 6, false] }],
          [{ 'color': [] }, { 'background': [] }],
          [{ 'font': [] }],
          [{ 'align': [] }],
          ['clean'],
          ['emoji'],
          ['link', 'image', 'video']
        ],
        handlers: {
          'emoji': function () {}
        }
      };



var editor = new Quill('#edit_post',
    {
         modules: {
    // toolbar: toolbarOptions

          "toolbar": toolbarOptions,
          "emoji-toolbar": true,
          "emoji-shortname": true,
          "emoji-textarea": true

  },toolbarOptions ,theme: 'snow',
    }

    );
var form = document.querySelector('form');
var about = document.querySelector('input[name=text]');
form.onsubmit = function() {
  // Populate hidden form on submit

  about.value = editor.root.innerHTML;

};


//var justHtml = editor.root.innerHTML;