from flask import (
    Flask,
    render_template,
    abort,
    jsonify,
    request,
    session,
    flash,
    json,
    redirect,
    url_for,
    Markup,
)
from Component.Form import RegisterForm
from config import Config
from flask_sqlalchemy import SQLAlchemy
from flask_sqlalchemy_session import flask_scoped_session
from model.create_table import session_factory, User, Post, Comment
from flask import send_from_directory
import sys

# pip3 install -r requirements.txt


def add_post(request1):
    post = Post(
        username=session.get("user")["username"],
        body=request1.form.get("text"),
        title=request1.form.get("title"),
        categories=request1.form.get("categories"),
    )
    scoped_session.add(post)
    try:
        scoped_session.commit()
    except:
        scoped_session.rollback()
        scoped_session.flush()


def add_comment(request1, artid):
    comm = Comment(
        post_id=artid,
        user_id=session.get("user")["username"],
        body=request1.form.get("commentBody"),
    )
    scoped_session.add(comm)
    try:
        scoped_session.commit()
    except:
        scoped_session.rollback()
        scoped_session.flush()


app = Flask(__name__)
app.config["SECRET_KEY"] = "password"
app.config.from_object("config.Config")
scoped_session = flask_scoped_session(session_factory, app)
db = SQLAlchemy(app)
techarticle = scoped_session.query(Post).filter_by(categories="Tech").first()
lifearticle = scoped_session.query(Post).filter_by(categories="Life").first()
travelartile = scoped_session.query(Post).filter_by(categories="Travel").first()


@app.route("/copy/<path:path>")
def copy(path):
    return path


@app.route("/client/src/<path:subfolder>/<path:img>")
def send_image(img, subfolder):
    return send_from_directory("./client/src/" + subfolder, img)


@app.route("/static/<path:fol>/<path:file>")
def send_static(file, fol):
    return send_from_directory("./client/build/static/" + fol, file)


@app.route("/service-worker.js")
def send_sw():
    return send_from_directory("./client/build/", "service-worker.js")


@app.route("/register", methods=("GET", "POST"))
def register():
    form = RegisterForm()
    return render_template("form.html", form=form)


@app.route("/submit", methods=("GET", "POST"))
def submit():
    form = RegisterForm()
    if form.validate_on_submit() and request.method == "POST":
        user = User(
            username=request.form.get("username"),
            email=request.form.get("email"),
            password=request.form.get("password"),
        )
        scoped_session.add(user)
        try:
            scoped_session.commit()
        except:
            scoped_session.rollback()
            scoped_session.flush()
        return render_template("registerSuccessful.html", user=user)
    return "not good"


@app.route("/", defaults={"path": ""})
def home(path):
    print(session.get("logged_in"))
    if session.get("logged_in"):
        user_name = session.get("user")["username"]
        techarticle1 = scoped_session.query(Post).filter_by(categories="Tech").first()
        lifearticle1 = scoped_session.query(Post).filter_by(categories="Life").first()
        travelartile1 = (
            scoped_session.query(Post).filter_by(categories="Travel").first()
        )
        return render_template(
            "home.html",
            user_name=user_name,
            techarticle=techarticle1,
            lifearticle=lifearticle1,
            travelartile=travelartile1,
        )
    return render_template(
        "home.html",
        techarticle=techarticle,
        lifearticle=lifearticle,
        travelartile=travelartile,
    )
    # return send_from_directory('./client/build', 'index.html')


# https://pythonspot.com/login-authentication-with-flask/
@app.route("/login", methods=["GET", "POST"])
def login():
    if request.method == "POST":
        error = None
        if not session.get("logged_in"):
            user = (
                scoped_session.query(User)
                .filter_by(email=request.form["email"])
                .first()
            )
            if not user:
                flash("user does not exist")
            elif user.password != request.form["password"]:
                print(user.password)
                flash("password not matched")
            else:
                session["logged_in"] = True
                session["user"] = user.serialize()
                return render_template(
                    "home.html",
                    techarticle=techarticle,
                    lifearticle=lifearticle,
                    travelartile=travelartile,
                )

            return render_template("login.html", error=error)
    if request.method == "GET":
        return render_template("login.html")


@app.route("/post_<path:num>")
def post(num):
    if num.isdigit():
        article = scoped_session.query(Post).filter_by(id=num).first()
    else:
        article = scoped_session.query(Post).filter_by(categories=num).first()
    comments = scoped_session.query(Comment).filter_by(post_id=article.id)
    article.view = article.view + 1
    scoped_session.commit()
    print(article.view, file=sys.stderr)
    return render_template("post.html", article=article, comments=comments)


@app.route("/logout")
def logout():
    session.clear()
    return redirect(url_for("home"))


@app.route("/protected", methods=["GET", "POST"])
def create_post():
    if request.method == "GET":
        if session.get("user")["username"] != "admin":
            abort(401)
        else:
            return render_template("add_post.html")

    if request.method == "POST":
        add_post(request)
        return render_template("add_post.html")


@app.route("/dsafadsfdsafer")
def view_all_posts():
    posts = scoped_session.query(Post)
    return render_template("view_posts.html", posts=posts)


@app.route("/add_comment_<path:num>", methods=["POST"])
def create_comment(num):
    add_comment(request, num)
    _article = scoped_session.query(Post).filter_by(id=num).first()
    comments = scoped_session.query(Comment).filter_by(post_id=_article.id)
    return render_template("post.html", article=_article, comments=comments)


@app.route("/delete_post_<path:id>")
def delete_post(id):
    dele = scoped_session.query(Post).filter_by(id=id).first()
    scoped_session.delete(dele)
    try:
        scoped_session.commit()
    except:
        scoped_session.rollback()
        scoped_session.flush()
    return redirect(url_for("view_all_posts"))


@app.route("/delete_comment_<path:id>_<path:num>", methods=["GET"])
def delete_comment(id, num):
    dele = scoped_session.query(Comment).filter_by(id=id).first()
    scoped_session.delete(dele)

    try:
        scoped_session.commit()
    except:
        scoped_session.rollback()
        scoped_session.flush()

    return redirect(url_for("post", num=num))


# @app.route('/<path:path>')
# def send_html(path):
#     path = path + '.html'
#     return render_template(path)


if __name__ == "__main__":
    app.debug = True
    app.run(debug=True)
#export FLASK_ENV=development