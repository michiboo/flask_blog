import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import NavBar from "./Components/NavBar";
import { BrowserRouter as Router, Route, Link} from 'react-router-dom'


const Home = () => (<div><h1>Welcome home</h1><Link to='/about'>Go to about</Link></div>)
const About = () => (<div><h1>About</h1><Link to='/'>Go home</Link></div>)
class App extends Component {
  render() {
    return (

        <Router>
    <div>
          <NavBar/>
    <Route path="/" component={Home} />
        <Route path="/about" component={About} />

     </div>
</Router>


    );
  }
}

export default App;
