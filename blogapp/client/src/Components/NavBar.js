import React from 'react';
import logo from '../image/logo.png';
import emologo from '../image/emo.png';
import techlogo from '../image/tech.png';
import travellogo from '../image/travel.png';
import lifelogo from '../image/life.png';
import aboutlogo from '../image/about.png';
import homelogo from '../image/home.png';
import {Link} from 'react-router-dom'
import './Nav1.css'
require('jquery');
require('bootstrap');


const About = () => (<div><h1>About</h1><Link to='/'>Go home</Link></div>);
const rootDomain =  window.location.hostname;

export default class NavBar extends React.Component {



  render() {
    return (

      <div>

<style>
@import url('https://fonts.googleapis.com/css?family=Comfortaa');
</style>
  <nav class="navbar navbar-light ">
  <Link to="/" class="navbar-brand" >
    <img src={logo}  width="20%" height="20%" alt="logo"/>
    </Link>
<a href="/register" > Register </a>



  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>



  <div class="collapse navbar-collapse" id="navbarNav">
    <ul class="navbar-nav" data-toggle="collapse" data-target="#navbarNav">
      <li class="nav-item">
        <Link to="/" class="nav-link"  >Home <img src={homelogo} class='logo' alt="logo"/>   </Link>

      </li>
      <li class="nav-item">
        <Link to="/" class="nav-link" >Tech<img src={techlogo} class='logo' alt="logo"/></Link>
      </li>
      <li class="nav-item">
        <Link to="/" class="nav-link" >Life<img src={lifelogo} class='logo' alt="logo"/></Link>
      </li>
      <li class="nav-item">
        <Link to="/" class="nav-link" >Travel <img src={travellogo} class='logo' alt="logo"/></Link>
      </li>
      <li class="nav-item">
        <Link to="/" class="nav-link" >Emo stuff <img src={emologo} class='logo' alt="logo"/></Link>
      </li>
      <li class="nav-item">
        <Link to="/about" class="nav-link" >about <img src={aboutlogo} class='logo' alt="logo"/></Link>
      </li>

    </ul>

    </div>

</nav>
      </div>
    );
  }
}