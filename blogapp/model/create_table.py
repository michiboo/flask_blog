from sqlalchemy import (
    create_engine,
    ForeignKey,
    CheckConstraint,
    Column,
    DateTime,
    Integer,
    String,
    Text,
)
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
import datetime

engine = create_engine("sqlite:///model/database.db", echo=True)
# engine = create_engine('sqlite:///database.db', echo=True)
Base = declarative_base(engine)
session_factory = sessionmaker(bind=engine)


class User(Base):
    __tablename__ = "User"
    __table_args__ = (CheckConstraint("email GLOB '*@*.*'"),)
    id = Column(Integer, primary_key=True)
    username = Column(String(200), unique=True, nullable=False)
    email = Column(String(200), unique=True, nullable=False)
    password = Column(String(200), nullable=False)
    created_date = Column(DateTime, default=datetime.datetime.utcnow)
    last_updated = Column(DateTime, onupdate=datetime.datetime.utcnow)

    def serialize(self):
        return {
            "id": self.id,
            "username": self.username,
            "email": self.email,
            "password": self.password,
            "create_date": self.created_date,
            "last_updated": self.last_updated,
        }


class Post(Base):
    __tablename__ = "Post"
    __table_args__ = (CheckConstraint("categories in ('Tech','Life','Travel','Emo')"),)
    id = Column(Integer, primary_key=True)
    username = Column(String(200), nullable=False)
    created_date = Column(DateTime, default=datetime.datetime.utcnow)
    body = Column(Text)
    title = Column(String(60), nullable=False)
    categories = Column(String(100), nullable=False)
    view = Column(Integer, default=0)

    def serialize(self):
        return {
            "id": self.id,
            "username": self.username,
            "create_date": self.created_date,
            "body": self.body,
            "title": self.title,
            "categories": self.categories,
            "view": self.view,
        }


class Comment(Base):
    __tablename__ = "Comment"
    id = Column(Integer, primary_key=True)
    post_id = Column(Integer, ForeignKey("Post.id"))
    user_id = Column(Integer, ForeignKey("User.id"))
    created_date = Column(
        DateTime, default=datetime.datetime.utcnow().replace(microsecond=0, second=0)
    )
    body = Column(Text)

    def serialize(self):
        return {
            "id": self.id,
            "parent_id": self.parent_id,
            "body": self.body,
            "user_id": self.user_id,
            "created_date": self.created_date,
        }


# try:
#     Base.metadata.drop_all()
# except:
#     pass
Base.metadata.create_all()
