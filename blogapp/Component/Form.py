from wtforms import BooleanField, StringField, PasswordField, validators, SubmitField
from flask_wtf import FlaskForm


class RegisterForm(FlaskForm):
    username = StringField("Enter your name", [validators.Length(min=4, max=100)])
    email = StringField("Email Address", [validators.Length(min=6, max=100)])
    confirm = PasswordField("Repeat Password", [validators.data_required()])
    password = PasswordField(
        "Password",
        validators=[
            validators.data_required(),
            validators.EqualTo("confirm", message="Passwords must match"),
        ],
    )

    accept_tos = BooleanField("I accept the TOS", [validators.data_required()])
